<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
        <label>First Name:</label><br>
        <input type="text" name="fname"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br>
        <input type="radio">Male <br>
        <input type="radio">Female <br>
        <input type="radio">Other <br><br>
        <label for="Nationality">Nationality:</label><br>
        <select name="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapore">Singapore</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="Language">Indonesia <br>
        <input type="checkbox" name="Language">English <br>
        <input type="checkbox" name="Language">Other <br><br>
        <label>Bio:</label><br>
        <textarea name="message" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>