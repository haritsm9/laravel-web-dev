<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function form(){
        return view('page.register');
    }
    public function wel(Request $request){
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('page.welcome', compact('namaDepan','namaBelakang'));
    }
}
