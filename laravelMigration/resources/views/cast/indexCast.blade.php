@extends('master')
@section('title')
    Cast <a href="/cast/create" class="btn btn-primary float-right">Add Data</a>
@endsection

@section('sub-title')
    Index Cast
@endsection
@section('content')


    <table class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
        <th scope="col">No.</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <th scope="row">{{ $key +1 }}</th>
            <td>{{$item -> nama}}</td>
            <td>{{$item -> umur}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                <a href="/cast/{{$item->id}}" class ="btn btn-primary btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class ="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
            <h1>Data Kosong</h1>
        @endforelse
        
        
    </tbody>
    </table>


@endsection