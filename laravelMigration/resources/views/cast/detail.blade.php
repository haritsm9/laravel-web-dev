@extends('master')
@section('title')
    Detail Cast
@endsection

@section('sub-title')
    {{$cast->nama}}
    <small class="text-muted ml-3">{{$cast->umur}} years old</small>
@endsection
@section('content')
    <p>{{$cast->bio}}</p>
    
@endsection