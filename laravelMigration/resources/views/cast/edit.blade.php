@extends('master')
@section('title')
    Cast
@endsection

@section('sub-title')
    Create Cast
@endsection
@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="name" name="nama" value="{{$cast->nama}}" class="form-control" >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="integer" name="umur" value="{{$cast->umur}}" class="form-control" >
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label><br>
        <input type="text" name="bio" value="{{$cast->bio}}" class="form-control" >
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection