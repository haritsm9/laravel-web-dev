@extends('master')
@section('title')
    Cast
@endsection

@section('sub-title')
    Create Cast
@endsection
@section('content')
    <form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="name" name="nama" class="form-control" >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="integer" name="umur" class="form-control" >
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label><br>
        <input type="text" name="bio" class="form-control" >
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection