<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="index.html">
              <i class="typcn typcn-home menu-icon"></i>
              <span class="menu-title">Home</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="typcn typcn-film menu-icon"></i>
              <span class="menu-title">Film</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="#">Genre</a></li>
                <li class="nav-item"> <a class="nav-link" href="/cast">Cast</a></li>
                <li class="nav-item"> <a class="nav-link" href="#">Review</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </nav>