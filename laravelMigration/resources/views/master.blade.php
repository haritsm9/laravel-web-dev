<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>PolluxUI Admin</title>
  <!-- css -->
  <link rel="stylesheet" href="{{asset('/template/vendors/typicons/typicons.css')}}">
  <link rel="stylesheet" href="{{asset('/template/vendors/css/vendor.bundle.base.css')}}">
  <link rel="stylesheet" href="{{asset('/template/css/vertical-layout-light/style.css')}}">
  <link rel="shortcut icon" href="{{asset('/template/images/favicon.png')}}" />
</head>
<body>
  <!-- -->
  <div class="container-scroller">
    <!-- partials navbar -->
    @include('partials.nav')
    <div class="container-fluid page-body-wrapper">
      <!-- partials settings  -->
      @include('partials.settings')
      <!-- partial sidebar -->
      @include ('partials.sidebar')
      <!-- main panel   -->
      <div class="main-panel">
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col mr-3">
                  <h1>@yield('title')</h1>
                </div>
              </div>
            </div>
            <!-- /.container-fluid -->
          </section>

          <!-- Main content -->
          <section class="content">

            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <h2 class="card-title mt-4 ml-3">@yield('sub-title')</h2>
              </div>
              <div class="card-body">
                @yield('content')
              </div>
            </div>
            <!-- /.card -->

          </section>
          <!-- /.content -->
        </div>
        <!-- content-wrapper ends -->
        <!-- footer -->
        <footer class="footer">
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 <a href="https://www.bootstrapdash.com/" class="text-muted" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center text-muted">Free <a href="https://www.bootstrapdash.com/" class="text-muted" target="_blank">Bootstrap dashboard</a> templates from Bootstrapdash.com</span>
                    </div>
                </div>    
            </div>        
        </footer>
      </div>
      <!-- main-panel ends -->
    </div>
      
    
  <!-- base:js -->
  <script src="{{asset('/template/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{asset('/template/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('/template/js/template.js')}}"></script>
  <script src="{{asset('/template/js/settings.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('/template/js/dashboard.js')}}"></script>
  <!-- End custom js for this page-->
</body>

</html>

