<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

// Create cast 
// route ke halaman form input cast 
Route::get('/cast/create', [CastController::class, 'create']);
// route untuk data baru ke tabel cast 
Route::post('/cast', [CastController::class, 'store']);

// Read data 
// menampilkan data dari db 
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{id}', [CastController::class, 'show']);

// Update Data 
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{id}', [CastController::class, 'update']);

// Delete Data 
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
